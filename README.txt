Insurance premiums calculator
Endpoint: /premium (http://localhost:8080/premium for local testing)
Content-type: application/json
as request body resources/TestRequest.json or resources/TestRequest2.json can be used
service returns ResponseObject in it's response, field yourInsurancePremium is updated with final value;

Constants are taken from application-mocks.yaml file
risk type definitions are in insurance.riskTypes section
defined HIGH and LOW borders for pricing.
if total insurance costs for risk type reaches HIGH border (pricing.multiplierDefinitions section of yaml),
premium multiplier changes to %riskType%_HIGH value (pricing.multipliers section)