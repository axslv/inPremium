package lv.axs.demo.service;

import lombok.RequiredArgsConstructor;
import lv.axs.demo.enitity.Policy;
import lv.axs.demo.enitity.PolicySubObject;
import lv.axs.demo.enitity.ResponseObject;
import lv.axs.demo.stub.InsurancePricingServiceStub;
import lv.axs.demo.util.Multipliers;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;

@Service
@RequiredArgsConstructor
public class InsurancePremiumsCalculatorService {

    private final InsurancePricingServiceStub pricingService;


    public ResponseObject calculatePremium(final Policy policy) {

        ResponseObject ro = new ResponseObject();
        ro.setPolicy(policy);
        Map<String, BigDecimal> sumsByRiskType = new HashMap<>();
        policy.getPolicyObjects().forEach(po -> {
            Map<String, List<PolicySubObject>> subObjectsByRiskType = po.getPolicySubObjects().stream().collect(groupingBy(PolicySubObject::getRiskType));

            subObjectsByRiskType.keySet().forEach(key -> {
                Optional<BigDecimal> riskSum = subObjectsByRiskType.get(key).stream().filter(Objects::nonNull).map(PolicySubObject::getAmountInsured)
                        .reduce(BigDecimal::add);
                sumsByRiskType.put(key, riskSum.get());
            });
        });

        Map<String, Double> multi = Multipliers.parseMultipliers(sumsByRiskType,
                pricingService.getPricingConstants().getMultiplierDefinitions(),
                pricingService.getPricingConstants().getMultipliers());

        BigDecimal totalPremium = new BigDecimal(0.00).setScale(2);
        List<BigDecimal> insurancePremiums = new ArrayList<>();

        multi.keySet().forEach(key -> {
            insurancePremiums.add(totalPremium.add(sumsByRiskType.get(key).multiply(new BigDecimal(multi.get(key)))).setScale(2, RoundingMode.HALF_UP));
        });

        ro.setYourInsurancePremium(insurancePremiums
                .stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add));
        return ro;
    }

}
