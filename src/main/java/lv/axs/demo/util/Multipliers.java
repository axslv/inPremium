package lv.axs.demo.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Multipliers {
    private static final String LOW = "LOW";
    private static final String HIGH = "HIGH";

    //this method parses multiplier definitions and multipliers from human-readable format.
    public static Map<String, Double> parseMultipliers(Map<String, BigDecimal> pricesByRiskType,
                                                       Map<String, String> multiplierDefinitions,
                                                       Map<String, Double> multipliers) {
        Map<String, Double> multipliersForSums = new HashMap<>();

        pricesByRiskType.keySet().forEach(riskType -> {
            String multiplierKey = fetchMultiplierKeyByRiskType(multiplierDefinitions.get(riskType), pricesByRiskType.get(riskType), riskType);
            multipliersForSums.put(riskType, multipliers.get(multiplierKey));
        });
        return multipliersForSums;
    }

    public static String fetchMultiplierKeyByRiskType(String priceBorderValue, BigDecimal sumForRiskType, String riskType) {
        String multiplierKey = "NO_KEY";
        if (priceBorderValue.contains("gt")) {
            String rep = priceBorderValue.replace("gt", "").trim();
            int compareResult = sumForRiskType.compareTo(new BigDecimal(rep));

            if (compareResult == 1) {
                multiplierKey = riskType + "_" + HIGH;
            } else {
                multiplierKey = riskType + "_" + LOW;
            }

        } else if (priceBorderValue.matches("\\d+")) {
            int compareResult = sumForRiskType.compareTo(new BigDecimal(priceBorderValue));
            if (compareResult == 1 || compareResult == 0) {
                multiplierKey = riskType + "_" + HIGH;
            } else {
                multiplierKey = riskType + "_" + LOW;
            }
        }
        return multiplierKey;
    }
}
