package lv.axs.demo.stub;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@RequiredArgsConstructor
@Data
public class InsurancePricingServiceStub {

    @Value("${insurance.riskTypes}")
    private final Set<String> riskTypes;

    private final PricingConstantsStub pricingConstants;

}
