package lv.axs.demo.stub;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "pricing")
@Data
public class PricingConstantsStub {

    private HashMap<String, Double> multipliers;
    private Map<String, String> multiplierDefinitions;

}