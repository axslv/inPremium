package lv.axs.demo.enitity;

import lombok.Data;

import java.util.Set;

@Data
public class Policy {
    private String policyUUID;
    private String policyNumber;
    private PolicyStatus policyStatus;
    private Set<PolicyObject> policyObjects;
}
