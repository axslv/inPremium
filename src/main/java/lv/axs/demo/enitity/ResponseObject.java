package lv.axs.demo.enitity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class ResponseObject {
    private Policy policy;
    private BigDecimal yourInsurancePremium;

    public ResponseObject(Policy policy) {

    }
}
