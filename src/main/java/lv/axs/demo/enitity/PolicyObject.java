package lv.axs.demo.enitity;

import lombok.Data;

import java.util.Set;

@Data
public class PolicyObject {

    private String policyObjectName;
    private Set<PolicySubObject> policySubObjects;

}
