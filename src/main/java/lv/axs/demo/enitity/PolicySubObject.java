package lv.axs.demo.enitity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PolicySubObject {

    private String subObjectName;
    private String riskType;
    private BigDecimal amountInsured;

}
