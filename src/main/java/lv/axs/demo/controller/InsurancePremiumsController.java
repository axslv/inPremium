package lv.axs.demo.controller;

import lombok.RequiredArgsConstructor;
import lv.axs.demo.enitity.Policy;
import lv.axs.demo.enitity.ResponseObject;
import lv.axs.demo.service.InsurancePremiumsCalculatorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class InsurancePremiumsController {

    private final InsurancePremiumsCalculatorService calculatorService;

    @PostMapping("/premium")
    public ResponseEntity<ResponseObject> calculatePremium(@RequestBody Policy policy) {
       return ResponseEntity.ok().body(calculatorService.calculatePremium(policy));
    }

}
