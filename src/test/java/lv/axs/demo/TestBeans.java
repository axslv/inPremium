package lv.axs.demo;

import lv.axs.demo.stub.InsurancePricingServiceStub;
import lv.axs.demo.stub.PricingConstantsStub;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@Import(PricingConstantsStub.class)
public class TestBeans {

    @Bean
    public InsurancePricingServiceStub insurancePricingServiceStub() {
        Set<String> riskTypes = new HashSet<>();
        riskTypes.add("FIRE");
        riskTypes.add("THEFT");

        HashMap<String, Double> multipliers = new HashMap<>();
        multipliers.put("FIRE_LOW", 0.014);
        multipliers.put("FIRE_HIGH", 0.024);
        multipliers.put("THEFT_LOW", 0.11);
        multipliers.put("THEFT_HIGH", 0.05);

        HashMap<String, String> multiplierDefinitions = new HashMap<>();
        multiplierDefinitions.put("FIRE", "gt 100");
        multiplierDefinitions.put("THEFT", "15");

        PricingConstantsStub pStub = new PricingConstantsStub();
        pStub.setMultipliers(multipliers);
        pStub.setMultiplierDefinitions(multiplierDefinitions);


        return new InsurancePricingServiceStub(riskTypes, pStub);
    }


}
