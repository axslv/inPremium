package lv.axs.demo;

import lv.axs.demo.util.Multipliers;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class MultipliersTest {

    private final Map<String, BigDecimal> pricesByRiskType = new HashMap<>();
    private final Map<String, String> multiplierDefinitions = new HashMap<>();
    private final Map<String, Double> multipliers = new HashMap<>();

    @Before
    public void init() {
        pricesByRiskType.put("FIRE", new BigDecimal(100.00));
        pricesByRiskType.put("THEFT", new BigDecimal(8.00));

        multiplierDefinitions.put("FIRE", "gt 100");
        multiplierDefinitions.put("THEFT", "15");

        multipliers.put("FIRE_LOW", 0.014);
        multipliers.put("FIRE_HIGH", 0.024);
        multipliers.put("THEFT_LOW", 0.11);
        multipliers.put("THEFT_HIGH", 0.05);
    }

    @Test
    public void shouldPutCorrectMultiplier() {
        Map<String, Double> multi = Multipliers.parseMultipliers(pricesByRiskType, multiplierDefinitions, multipliers);
        assertThat(multi.keySet().size(), is(2));
        assertThat(multi.get("FIRE"), is(0.014)); //should pick FIRE_LOW multiplier, cause price is 100.00
        assertThat(multi.get("THEFT"), is(0.11)); //should pick THEFT_LOW multiplier, cause price is 8.00
        pricesByRiskType.put("FIRE", new BigDecimal(101.00));
        multi = Multipliers.parseMultipliers(pricesByRiskType, multiplierDefinitions, multipliers);
        assertThat(multi.get("FIRE"), is(0.024)); //should pick FIRE_HIGH multiplier, cause price now is 101.00
    }

    @Test
    public void shouldReturnValidKey() {
        String multiplierKey = Multipliers.fetchMultiplierKeyByRiskType("gt 100", new BigDecimal(100).setScale(2, RoundingMode.HALF_UP), "FIRE");
        assertThat(multiplierKey, is("FIRE_LOW"));
    }

    @Test
    public void shouldReturnValidKey1() {
        String multiplierKey = Multipliers.fetchMultiplierKeyByRiskType("gt 100", new BigDecimal(101).setScale(2, RoundingMode.HALF_UP), "FIRE");
        assertThat(multiplierKey, is("FIRE_HIGH"));
    }

    @Test
    public void shouldReturnValidKey2() {
        String multiplierKey = Multipliers.fetchMultiplierKeyByRiskType("15", new BigDecimal(14).setScale(2, RoundingMode.HALF_UP), "THEFT");
        assertThat(multiplierKey, is("THEFT_LOW"));
    }

    @Test
    public void shouldReturnValidKey3() {
        String multiplierKey = Multipliers.fetchMultiplierKeyByRiskType("15", new BigDecimal(16).setScale(2, RoundingMode.HALF_UP), "THEFT");
        assertThat(multiplierKey, is("THEFT_HIGH"));
    }


}
