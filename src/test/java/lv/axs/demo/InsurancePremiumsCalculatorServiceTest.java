package lv.axs.demo;

import lv.axs.demo.enitity.Policy;
import lv.axs.demo.enitity.PolicyObject;
import lv.axs.demo.enitity.PolicyStatus;
import lv.axs.demo.enitity.PolicySubObject;
import lv.axs.demo.service.InsurancePremiumsCalculatorService;
import lv.axs.demo.stub.InsurancePricingServiceStub;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={TestBeans.class})
public class InsurancePremiumsCalculatorServiceTest {

    @Autowired
    private InsurancePricingServiceStub stubService;
    private InsurancePremiumsCalculatorService service;

    private final Policy policy = new Policy();

    private static final double PREMIUM_FIRST_VALID_CASE = 2.28;

    //this could be parsed from json
    @Before
    public void init() {
        policy.setPolicyNumber("PolNum");
        policy.setPolicyUUID("UniqueId");
        policy.setPolicyStatus(PolicyStatus.REGISTERED);

        PolicySubObject psub = new PolicySubObject();
        psub.setAmountInsured(new BigDecimal(100.00));
        psub.setRiskType("FIRE");
        psub.setSubObjectName("name1");

        PolicySubObject psub1 = new PolicySubObject();
        psub1.setAmountInsured(new BigDecimal(8.00));
        psub1.setRiskType("THEFT");
        psub1.setSubObjectName("name2");

        Set<PolicySubObject> subObjects = new HashSet<>();
        subObjects.add(psub);
        subObjects.add(psub1);

        PolicyObject po = new PolicyObject();
        po.setPolicyObjectName("PolicyObjectName");
        po.setPolicySubObjects(subObjects);
        Set<PolicyObject> policyObjects = new HashSet<>();
        policyObjects.add(po);
        policy.setPolicyObjects(policyObjects);

        service = new InsurancePremiumsCalculatorService(stubService);
    }


    @Test
    public void shouldReturnValidPremium() {
        BigDecimal yourInsurancePremium = service.calculatePremium(policy).getYourInsurancePremium();
        assertThat(yourInsurancePremium.compareTo(new BigDecimal(PREMIUM_FIRST_VALID_CASE).setScale(2, RoundingMode.HALF_UP)), is(0));
    }


}
